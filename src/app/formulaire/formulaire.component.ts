import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

interface Item {
  type: string;
  value: any[];
}

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.scss']
})
export class FormulaireComponent implements OnInit {
  constructor(private fb: FormBuilder) {}

  form: FormGroup;

  select1: Item[] = [
    { type: 'bois', value: ['Arme en bois', 'TV en bois', 'Ecole en bois'] },
    { type: 'fer', value: ['Arme en fer', 'Pistolet en fer', 'Table en fer'] }
  ];

  typeSelected: string;
  ngOnInit() {
    this.form = this.fb.group({
      firstInput: ['Text1'],
      secondInput: ['Text2'],
      list: []
    });
  }

  getValue(type: string) {
    if (type) {
      const elt = this.select1.find((value: Item) => value.type === type);
      return elt.value;
    }
  }

  setTypeSelected(type: string) {
    console.log('type', this.getValue(type));

    this.typeSelected = type;
  }
  showResult() {
    console.log('formulaire', this.form);
  }
}
